//
//  MainPageTableViewCell.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/4/2.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

import UIKit

class MainPageTableViewCell: UITableViewCell {

    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var sectionNameCell: UILabel!
    @IBOutlet weak var sectionDescriptionCell: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
