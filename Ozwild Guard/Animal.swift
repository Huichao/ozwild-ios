//
//  Animal.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/4/22.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

import UIKit

// Class for an instance of an animal type
class Animal
{
    var animalName:String!
    var scienceName:String!
    var commonName:String!
    var longitude:Double!
    var latitude:Double!
    var dangerousLevel:String!
    var dangerousDes:String!
    var description:String!
    
}
