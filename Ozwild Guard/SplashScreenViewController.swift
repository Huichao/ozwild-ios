//
//  SplashScreenViewController.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/4/6.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

import UIKit

// This class is used to manage the splash screen (set the time interval of the splash screen
class SplashScreenViewController: UIViewController {

    // This function is invoked when the view is loaded
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set the displaying time of the splash screen to 2 seconds
        perform(#selector(SplashScreenViewController.showNavController), with: nil, afterDelay: 2)
        // Do any additional setup after loading the view.
    }
    
    // It will be called after the 2-second-display and navigate to another view
    func showNavController()
    {
        performSegue(withIdentifier: "showSplashScreenSegue", sender: self)
    }

    // Display warnings about the memory
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
