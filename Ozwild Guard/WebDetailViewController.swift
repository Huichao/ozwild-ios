//
//  WebDetailViewController.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/4/13.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

import UIKit

// This is the web displaying page after clicking a certain row in the first screen. It will open the web page for the user to browse.
class WebDetailViewController: UIViewController {

    @IBOutlet weak var webDetailField: UIWebView!
    
    // Used to store the url of the web
    var url:String?
    
    // This function is invoked when the view is loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let barColor = UIColor(red: 255/255, green:128/225, blue: 0/255, alpha: 1)
        self.navigationController?.navigationBar.barTintColor = barColor

        if let url = NSURL(string: self.url!)
        {
            let request = NSURLRequest(url: url as URL)
            webDetailField.loadRequest(request as URLRequest)
        }
        // Do any additional setup after loading the view.
    }

    // Display warnings about the memory
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // When the cancel button is clicked, the screen will go to the previous page.
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
