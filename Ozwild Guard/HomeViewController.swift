//
//  HomeViewController.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/4/13.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

import UIKit

// This view is the first page of the application. It will automatically retrieve data from the database we built online and display the news stored.
class HomeViewController: UIViewController, UITableViewDelegate
, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var imageField: UIImageView!
    @IBOutlet weak var webDescribeField: UITableView!
    @IBOutlet weak var activityWait: UIActivityIndicatorView!
    
    // Used to store the URL of the web
    var webURL:[String] = []
    // Used to store the URL of the image of the web
    var webImage:[String] = []
    // Used to store the title of the web
    var webTitle:[String] = []
    
    // This function is invoked when the view is loaded
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the color of the navigation bar
        let barColor = UIColor(red: 255/255, green:128/225, blue: 0/255, alpha: 1)
        self.navigationController?.navigationBar.barTintColor = barColor
        
        webDescribeField.delegate = self
        webDescribeField.dataSource = self
        webDescribeField.sectionHeaderHeight = 0
        webDescribeField.sectionFooterHeight = 0

        self.imageField.image = UIImage.init(named: "newsTitle")
        // Do any additional setup after loading the view.
        
        // Once the page is loaded, this function will be invoked to retrieve the news data from the dtabase
        readNewsData()
        

    }

    // Display warnings about the memory
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Set the height of each section.
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    // Set the number of sections for this table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // Set the number of rows for each secrtion
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0
        {
            return webURL.count
        }else
        {
            return 0
        }
    }
    
    // Set the cell details for each row in each section
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.webDescribeField.dequeueReusableCell(withIdentifier: "WebDescribeCell") as! WebDetailTableViewCell
        cell.textField.text = webTitle[indexPath.row]
        
        let catPictureURL = URL(string: webImage[indexPath.row])!
        // Creating a session object with the default configuration.
        let session = URLSession(configuration: .default)
        // Define a download task. The download task will download the contents of the URL as a Data object.
        let downloadPicTask = session.dataTask(with: catPictureURL) { (data, response, error) in
            // The download has finished.
            if let e = error {
                print("Error downloading cat picture: \(e)")
            }
            else
            {
                if let imageData = data {
                    // Convert that Data into an image
                    cell.imageField.image = UIImage(data: imageData)
                    // Do something with your image.
                }
                else
                {
                    print("Couldn't get image: Image is nil")
                }
            }
            // Set a delay for the session so that there's enough time for the retrieving process
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.webDescribeField.reloadData()
                
            }
        }
        downloadPicTask.resume()
        return cell
    }
    
    // When the cell is clicked, transfer the data to another view and navigate to that view
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowWebPageSegue"
        {
            let controller = segue.destination.childViewControllers[0] as! WebDetailViewController
            if let selectedCell = sender as? UITableViewCell {
                let indexPath = webDescribeField.indexPath(for: selectedCell)!
                let selectedItem = webURL[indexPath.row]
                controller.url = selectedItem
            }
        }
    }
    
    // Set the height for each row of the table
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0
        {
            return 110
        }
        return UITableViewAutomaticDimension
    }
    
    // When the 'refresh' button is clicked, the readNewsData() will be invoked to refresh the view
    @IBAction func refresh(_ sender: UIBarButtonItem) {
        readNewsData()
    }
    
    // Set the alert function, so that it can be called when this function is called
    func showAlertWithDismiss(title:String, message:String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertDismissAction = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alertController.addAction(alertDismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // This function is called when the view is loaded. It will read data from the database we built from a host and then store them into global variables
    func readNewsData(){
        self.activityWait.isHidden = false
        self.activityWait.startAnimating()
        self.view.isUserInteractionEnabled = false
        
        self.webURL = []
        self.webImage = []
        self.webTitle = []
        
        let url = NSURL(string: "http://ozwild.000webhostapp.com/news.php")!
        let urlRequest = NSURLRequest(url: url as URL)
        let session = URLSession.shared
        let result = session.dataTask(with: urlRequest as URLRequest) {
            (data, response, error) in
            // Async request, write code inside this handler once data has been processed
            do {
                // if no data is being received
                if data == nil {
                    self.showAlertWithDismiss(title: "Error", message: "Server connection error!")
                    return
                }
                // If there is only one group of data sent, which is not a NSArray, this would cause exception
                let anyObj = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                
                print("\(anyObj)")
                let arrayA = anyObj["news"] as! NSArray
                var index = 0
                while index < arrayA.count
                {
                    let dictionary = arrayA[index] as! NSDictionary
                    self.webURL.append(dictionary["news_url"] as! String)
                    self.webImage.append(dictionary["news_img_url"] as! String)
                    self.webTitle.append(dictionary["news_title"] as! String)
                    index = index + 1
                }
                
                let when = DispatchTime.now() + 0.01
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.webDescribeField.reloadData()
                    self.view.reloadInputViews()
                    self.activityWait.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    self.activityWait.isHidden = true
                }
            } catch {
                print("json error: \(error)")
            }
        }
        result.resume()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
