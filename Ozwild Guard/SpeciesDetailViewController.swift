//
//  SpeciesDetailViewController.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/3/27.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

import UIKit

// This view will be generated when the cell in the 'searchByLoction' view is selected. It will display the detailed information about that particular species.
class SpeciesDetailViewController: UIViewController {
    
    // Used to stored the common name of the species
    var speciesCommonName:String = ""
    // Used to stored the scientific name of the species
    var speciesScientificName:String = ""
    // Used to stored the dangerous description of the species
    var speciesDangerousDes:String = ""
    // Used to stored the description of the species
    var speciesDes:String = ""
    
    @IBOutlet weak var commonNameField: UITextView!
    @IBOutlet weak var scientificNameField: UITextView!
    @IBOutlet weak var dangerousLevelField: UITextView!
    @IBOutlet weak var descriptionField: UITextView!
    @IBOutlet weak var speciesImageField: UIImageView!
    
    // This function is invoked when the view is loaded
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set the border style of all textboxes
        let color = UIColor.lightGray
        commonNameField.layer.borderColor = color.cgColor
        commonNameField.layer.borderWidth = 0.2
        scientificNameField.layer.borderColor = color.cgColor
        scientificNameField.layer.borderWidth = 0.2
        dangerousLevelField.layer.borderColor = color.cgColor
        dangerousLevelField.layer.borderWidth = 0.2
        descriptionField.layer.borderColor = color.cgColor
        descriptionField.layer.borderWidth = 0.2
        
        // Set the color of the navigation bar
        let barColor = UIColor(red: 255/255, green:128/225, blue: 0/255, alpha: 1)
        self.navigationController?.navigationBar.barTintColor = barColor
        
        // Disable the user interation with the screen
        self.view.isUserInteractionEnabled = false
        
        scientificNameField.text = speciesScientificName
        commonNameField.text = speciesCommonName
        dangerousLevelField.text = speciesDangerousDes
        descriptionField.text = speciesDes
        speciesImageField.image = UIImage.init(named: speciesScientificName)
    }

    // Display warnings about the memory
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // This function is called when the 'back' button is clicked and goes to the previous page.
    @IBAction func done(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}
