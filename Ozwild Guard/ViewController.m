//
//  SearchByLocationViewController.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/4/12.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

// Reference: https://github.com/dataminr/DTMHeatmap
// Reference: https://github.com/johndpope/TranSafe


// This view is used to generate the heatmap for all animal occurances stored in the database. Data will be read from the txt file installed with the application
#import "ViewController.h"
#import "DTMHeatmapRenderer.h"
#import "DTMDiffHeatmap.h"

// Define a particular color so that it can be used later
#define barTintColor [UIColor colorWithRed:255/255.0 green:128/255.0 blue:0/255.0 alpha:1.0]

@interface ViewController ()
@property (strong, nonatomic) DTMHeatmap *heatmap;
@property (strong, nonatomic) DTMDiffHeatmap *diffHeatmap;
@end

@implementation ViewController

// This function is invoked when the view is loaded (set the color of the navigation bar and initialize the heatmap for further use
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupHeatmaps];
    [self.navigationController.navigationBar setBarTintColor:barTintColor];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
}

// Configure the heatmap. Set the focus point of the map when it first initialized. Also, loading the snake heatmap when the map first initialized by default
- (void)setupHeatmaps
{
    // Set map region
    MKCoordinateSpan span = MKCoordinateSpanMake(1.0, 1.0);
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(-37.884263978375, 145.063381260396);
    self.mapView.region = MKCoordinateRegionMake(center, span);
    
    self.heatmap = [DTMHeatmap new];
    // Read the file
    [self.heatmap setData:[self parseLatLonFile:@"snakes"]];
    // Add overlay for all the snakes occurances
    [self.mapView addOverlay:self.heatmap];
}

// This function is called to read txt file
- (NSDictionary *)parseLatLonFile:(NSString *)fileName
{
    NSMutableDictionary *ret = [NSMutableDictionary new];
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName
                                                     ofType:@"txt"];
    NSString *content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    NSArray *lines = [content componentsSeparatedByString:@"\n"];
    for (NSString *line in lines) {
        NSArray *parts = [line componentsSeparatedByString:@","];
        NSString *latStr = parts[0];
        NSString *lonStr = parts[1];
        
        CLLocationDegrees latitude = [latStr doubleValue];
        CLLocationDegrees longitude = [lonStr doubleValue];
        
        // For this example, each location is weighted equally
        double weight = 1;
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:latitude
                                                          longitude:longitude];
        MKMapPoint point = MKMapPointForCoordinate(location.coordinate);
        NSValue *pointValue = [NSValue value:&point
                                withObjCType:@encode(MKMapPoint)];
        ret[pointValue] = @(weight);
    }
    
    return ret;
}

// This function is used when the segment control is selected and display the occurances of different animals
- (IBAction)segmentedControlValueChanged:(UISegmentedControl *)sender
{
    switch (sender.selectedSegmentIndex) {
        case 0:
            [self.mapView removeOverlay:self.diffHeatmap];
            [self.heatmap setData:[self parseLatLonFile:@"snakes"]];
            [self.mapView addOverlay:self.heatmap];
            [self.mapView reloadInputViews];
            break;
        case 1:
            [self.mapView removeOverlay:self.diffHeatmap];
            [self.heatmap setData:[self parseLatLonFile:@"spiders"]];
            [self.mapView addOverlay:self.heatmap];
            break;
        case 2:
            [self.mapView removeOverlay:self.diffHeatmap];
            [self.heatmap setData:[self parseLatLonFile:@"bees"]];
            [self.mapView addOverlay:self.heatmap];
            break;
        case 3:
            [self.mapView removeOverlay:self.diffHeatmap];
            [self.heatmap setData:[self parseLatLonFile:@"ants"]];
            [self.mapView addOverlay:self.heatmap];
            break;
        case 4:
            [self.mapView removeOverlay:self.diffHeatmap];
            [self.heatmap setData:[self parseLatLonFile:@"wasps"]];
            [self.mapView addOverlay:self.heatmap];
            break;
        case 5:
            [self.mapView removeOverlay:self.diffHeatmap];
            [self.heatmap setData:[self parseLatLonFile:@"Carnivorous Marsupials"]];
            [self.mapView addOverlay:self.heatmap];
            break;
    }
}

#pragma mark - MKMapViewDelegate
- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    return [[DTMHeatmapRenderer alloc] initWithOverlay:overlay];
}

@end
