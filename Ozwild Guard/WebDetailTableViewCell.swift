//
//  WebDetailTableViewCell.swift
//  Ozwild Guard
//
//  Created by 吴慧超 on 2017/4/13.
//  Copyright © 2017年 Smarty Roos. All rights reserved.
//

import UIKit

class WebDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var imageField: UIImageView!
    @IBOutlet weak var textField: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
